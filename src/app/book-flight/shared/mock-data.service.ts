import { Injectable } from '@angular/core';
import { FormGroup,FormBuilder } from '@angular/forms';
import { Flight } from 'src/app/flight';
import { Data } from './data';
@Injectable({
  providedIn: 'root'
})
export class MockDataService {
  FlightData:Array<Flight>

  constructor() {
    this.FlightData = Data.DataAll;
  }

  getData()
  {
    return this.FlightData;
  }
  onSubmit(f:Flight): void{
this.FlightData.push(f);
  }
}

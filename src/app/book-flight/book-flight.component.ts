import { invalid } from '@angular/compiler/src/render3/view/util';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup ,Validators} from '@angular/forms';
import { Flight } from '../flight';
import { MockDataService } from './shared/mock-data.service';
@Component({
  selector: 'app-book-flight',
  templateUrl: './book-flight.component.html',
  styleUrls: ['./book-flight.component.css']
})
export class BookFlightComponent implements OnInit {
  flight:Flight;
  bookForm!: FormGroup;
  bookFlights:Array<Flight>=[];


  constructor(private fb: FormBuilder,public MockService:MockDataService) {

    this.bookForm=this.fb.group({
      fullname:['',[Validators.required]],
      from:['',[Validators.required]],
      to:['',[Validators.required]],
      type:["",[Validators.required]],
      adults:[0,[Validators.required,Validators.pattern("[0-9]+"), Validators.min(1)]],
      departure:['', [Validators.required]],
      children:['', [Validators.required, Validators.pattern('[0-9]+'), Validators.min(0)]],
      infants:['',[Validators.required, Validators.pattern('[0-9]+'), Validators.min(0)]],
      arrival:['',[Validators.required]]

    });
    this.flight=new Flight("","","","",0,new Date(),0,0,new Date())

    this.bookFlights = MockService.getData()

   }

  ngOnInit(): void {
  }
  CheckLocation(f:FormGroup):boolean
  {
    if(f.get("from")?.value.toString() !== f.get("to")?.value.toString() || f.get("from")?.value.toString() === "" || f.get("to")?.value.toString() === "")
    {
      return false
    }
    return true
  }
  CheckDay(f:FormGroup):boolean
  {
    let Date1:Date = new Date(f.get("departure")?.value)
    let Date2:Date = new Date(f.get("arrival")?.value)
    if(Date2.getTime()<=Date1.getTime())
    {
      return true;
    }
    return false;
  }
 
  onclick(){

  }
  onSubmitForm(f:FormGroup): void{
    let form_record = new Flight(f.get('fullname')?.value,
                                  f.get('from')?.value,
                                  f.get('to')?.value,
                                  f.get('type')?.value,
                                  f.get('adults')?.value,
                                  f.get('departure')?.value,
                                  f.get('children')?.value,
                                  f.get('infants')?.value,
                                  f.get('arrival')?.value,)
    this.MockService.onSubmit(form_record) ;

  }



}
